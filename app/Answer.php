<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public function topics()
    {
        return $this ->belongsTo('App\Topic');
    }
}
