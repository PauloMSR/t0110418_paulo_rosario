<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->increments('id');
            $table -> string('title');
            $table-> boolean('active');
            $table-> unsignedInteger('creatorid');
            $table->timestamps();
            $table->foreign('creatorid')-> references('id')->on('users');
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}
