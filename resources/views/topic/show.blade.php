
@extends('layouts.app')

@section('content')

<div>
    <h3 style="margin-left:15%;margin-bottom: 2%">{{$topic->title}}</h3>

    @foreach($topic->answers as $answer)
        <li class="list-group-item" style="width:70%; margin: auto; height:60%">{{ $answer->answer }}</li>
    @endforeach

</div>

<a href="/answers/create" class="btn btn-info" style="float: right;margin-right: 15%;margin-top: 2%" role="button">Add Answer</a>

@endsection