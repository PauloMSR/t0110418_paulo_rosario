<h3>Topics</h3>


@foreach($topics as $topic)
    <form method="post" action="/topics/{{$topic->id}}/">
        <p>{{$topic->title}}</p>
        <a href="/topics/{{$topic->id}}">Edit</a>
        <input type="submit" value="Delete" name="{{$topic->id}}">
        @csrf
        @method('DELETE')
    </form>

@endforeach

<a href="#"> Add Topic</a>