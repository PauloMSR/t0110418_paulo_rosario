@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Welcome  to our Forum !! Please choose a topic.
                </div>
            </div>
        </div>
    </div>
</div>
 <br>
<br>





    <div>
        <h3 style="margin-left:15%;margin-bottom: 2%">Topics</h3>

        <ul class="list-group">
            @foreach($topics as $topic)
                <li class="list-group-item" style="width:70%; margin: auto; height:60%">{{$topic->title}} @if($topic->active == 1 )<a href="/topics/{{$topic->id}}" class="btn btn-success" style="float: right;margin-right:5%;margin:auto " role="button">Open</a>@else<a href="#" class="btn btn-danger" style="float: right;margin-right: 5%;margin:auto" role="button">Closed</a> @endif </li>
            @endforeach
        </ul>
    </div>


<a href="/topics/create" class="btn btn-info" style="float: right;margin-right: 15%;margin-top: 2%" role="button">Add Topic</a>

@endsection
